import { Component } from '@angular/core';
import { Product } from './product';
import {ProductsListComponent} from "./products-list/products-list.component";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  products: Product[];

  constructor(){
    this.products = [
      new Product("MYSHOES", "Black Running Shoes", "/assets/images/products/black-shoes.jpg", ["Men", "Shoes", "Runnig Shoes"], 109.99),
      new Product("NEATOJACKET", "Blue Jacket", "/assets/images/products/blue-jacket.jpg", ["Women", "Apparel", "Jackets & Vests"], 238.99),
      new Product("NICEHAT", "A nice black hat", "/assets/images/products/black-hat.jpg", ["Men", "Acessories", "Hats"], 29.99),
    ]
  }

  productWasSelected(product: Product) {
    console.log("Product clicked: ", product);
  }
}
